import CartParser from './CartParser';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	describe('parse', () => {
		it('should throw validation error, if csv file has invalid contents', () => {
			parser.readFile = jest.fn()
				.mockReturnValue(`Error,Price,Quantity
					Mollis consequat,9.00,2
					Tvoluptatem,10.32,1
				`)

			expect(() => { parser.parse('') }).toThrow('Validation failed!');
		})
	})

	describe('validate', () => {
		it('should return empty array, if contents are valid', () => {
			const contents ='Product name,Price,Quantity\n Mollis consequat,9.00,2';
			expect(parser.validate(contents)).toEqual([]);
		})

		it('should return array with error objects, if contents are invalid', () => {
			const contents = 'name,Price,Quantity\n Mollis consequat,Error, 2';
			const validationErrors = parser.validate(contents);

			expect(validationErrors).toBeInstanceOf(Array);
			expect(validationErrors[0]).toBeInstanceOf(Object);
		})

		it('should return array with error object, if contents have invalid header name', () => {
			let contents = `name,Price,Quantity`;
			let error = {
				column: 0,
				message: 'Expected header to be named "Product name" but received name.',
				row: 0,
				type: 'header'
			};
			expect(parser.validate(contents)).toContainEqual(error);

			contents = `Product name,Other,Quantity`;
			error = {
				column: 1,
				message: 'Expected header to be named "Price" but received Other.',
				row: 0,
				type: 'header'
			};
			expect(parser.validate(contents)).toContainEqual(error);
		})

		it('should return array with error object, if not enough cells in row', () => {
			const contents = 'Product name,Price,Quantity\n Mollis consequat,9.00';
			const error = {
				column: -1,
				message: 'Expected row to have 3 cells but received 2.',
				row: 1,
				type: 'row'
			};
			expect(parser.validate(contents)).toContainEqual(error);
		})

		it('should return array with error object, if value of string cell is empty', () => {
			const contents = 'Product name,Price,Quantity\n ,9.00,2';
			const error = {
				column: 0,
				message: 'Expected cell to be a nonempty string but received "".',
				row: 1,
				type: 'cell'
			};
			expect(parser.validate(contents)).toContainEqual(error);
		})

		it('should return array with error object, if value of number cell isn\'t a positive number', () => {
			let contents = 'Product name,Price,Quantity\n Mollis consequat,Error,2';
			let error = {
				column: 1,
				message: 'Expected cell to be a positive number but received "Error".',
				row: 1,
				type: 'cell'
			};
			expect(parser.validate(contents)).toContainEqual(error);

			contents = 'Product name,Price,Quantity\n Mollis consequat,9.00,-2';
			error = {
				column: 2,
				message: 'Expected cell to be a positive number but received "-2".',
				row: 1,
				type: 'cell'
			};
			expect(parser.validate(contents)).toContainEqual(error);
		})
	})

	describe('parseLine', () => {
		it('should return object with valid keys', () => {
			const csvLine = 'Mollis consequat,9.00,2';
			const item = parser.parseLine(csvLine);

			expect(item).toBeInstanceOf(Object);
			expect(item).toHaveProperty('name');
			expect(item).toHaveProperty('price');
			expect(item).toHaveProperty('quantity');
			expect(item).toHaveProperty('id');
			expect(Object.keys(item).length).toBe(4);
		})

		it('should return object with valid value types', () => {
			const csvLine = 'Mollis consequat,9.00,2';
			const item = parser.parseLine(csvLine);

			expect(typeof item.name).toBe('string');
			expect(typeof item.price).toBe('number');
			expect(typeof item.quantity).toBe('number');
			expect(typeof item.id).toBe('string');
		})
	})

	describe('calcTotal', () => {
		it('should return correct total', () => {
			let items = [
				{ name: 'Mollis consequat', price: 9, quantity: 2, id: '1' },
				{ name: 'Tvoluptatem', price: 10.32, quantity: 1, id: '2' },
				{ name: 'Scelerisque lacinia', price: 18.90, quantity: 1, id: '3' }
			]
			let mockTotal = 47.22;
			let total = parser.calcTotal(items);
			expect(total).toBeCloseTo(mockTotal);

			items = [
				{ name: 'Mollis consequat', price: 4, quantity: 5, id: '1' },
				{ name: 'Tvoluptatem', price: 8.99, quantity: 1, id: '2' },
				{ name: 'Scelerisque lacinia', price: 3, quantity: 2, id: '3' }
			]
			mockTotal = 34.99;
			total = parser.calcTotal(items);
			expect(total).toBeCloseTo(mockTotal);
		})
	})

	describe('createError', () => {
		it('should return object with valid keys', () => {
			const error = parser.createError(
				parser.ErrorType.HEADER,
				0,
				0,
				`Expected header to be named "Product name" but received name.`
			)

			expect(error).toBeInstanceOf(Object);
			expect(error).toHaveProperty('type');
			expect(error).toHaveProperty('row');
			expect(error).toHaveProperty('column');
			expect(error).toHaveProperty('message');
			expect(Object.keys(error).length).toBe(4);
		})
	})
});

describe('CartParser - integration test', () => {
	it('parse should return object with items array and total sum number', () => {
		const result = parser.parse('samples/cart.csv');

		expect(result).toBeInstanceOf(Object);
		expect(result).toHaveProperty('items');
		expect(result).toHaveProperty('total');
		expect(result.items).toBeInstanceOf(Array);
		expect(typeof result.total).toBe('number');
		expect(Object.keys(result).length).toBe(2);
	})
});
